﻿//Shai Guiterman

using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ServerApp
{
    public class ProccessTag
    {
        public delegate void SendTrigger(string msg);
        public event SendTrigger onSendTrigger;

        public delegate void ErrorEvent(string msg,string station,bool release);
        public event ErrorEvent onErrorEvent;

        public delegate void LogEvent(string msg, string station);
        public event LogEvent onLogEvent;

        public string visitorFolder { get; set; }

        //public string serverURL = "https://api.darksky.net/forecast/349783c1adafde506efb1b27d8377851/32.1093,34.8554?exclude=minutely,hourly,daily,alerts,flags&lang=en&units=si";


        // private string folder;


        public void NewTag(string station, string tag,string apiUrl,string action,string mediaFolder)
        {


            //onSendTrigger.Invoke("GetReady#" + station);

            visitorFolder = GetFolder(apiUrl, tag,station); //Get User's folder

            if (!visitorFolder.Contains("error"))
            {
                onSendTrigger.Invoke("Blink#" + station); //RPI needs to start blinking to choose a mode
                onLogEvent.Invoke("Found Folder " + visitorFolder, station);
                UpdateCounter(apiUrl, tag,station); //Update station count.
                CheckFolder(mediaFolder, visitorFolder); //Check if folder exists and creates one if it doesn't
              //  Send(station, action, mediaFolder + folder); //send trigger to touch and to RPI to start countdown
            }
            else
            {
                //needs to send an Error msg to RPI
                onSendTrigger.Invoke("ERROR#" + station);
                onLogEvent.Invoke("Didn't find tag", station);
            }

           
        }

        private string GetFolder(string _url,string _tag,string _station)
        {
            string folder = "error";
            string jsonString = GetJson(_url + "?method=retrieve&RFID=" + _tag, _station);
               
            try
            {
                dynamic stuff = JsonConvert.DeserializeObject(jsonString);
                if (stuff != null)
                {
                    if (stuff.status == "success")
                    {
                        folder = stuff.message;
                    }
                    else
                    {
                        folder = "error - no success";
                        onErrorEvent("GetFolder Error - " + stuff.message, _station,true);
                    }
                }

            }
            catch (Exception ex)
            {
                onErrorEvent("GetFolder Error - " + ex.Message.ToString(), _station,true);
                folder = "error";
            }

            return folder;
        }
        
        private void UpdateCounter(string _url, string _tag, string _station)
        {
           
            string jsonString = GetJson(_url + "?method=update&RFID=" + _tag + "&station=" + _station, _station);

            try
            {
                dynamic stuff = JsonConvert.DeserializeObject(jsonString);
                if (stuff != null)
                {
                    if (stuff.status == "success")
                    {
                        onLogEvent.Invoke("Updated counter", _station);
                        //  onErrorEvent(" " + stuff.messege);
                        // response = stuff.messege;
                    }
                    else
                    {

                      //  response = "error - " + stuff.messege;
                        onErrorEvent("UpdateCounter Error - " + stuff.messege, _station,false);
                    }
                }

            }
            catch (Exception ex)
            {
                onErrorEvent("UpdateCounter Error - " + ex.Message.ToString(), _station,false);
               // response = "error";
            }

           //return response;
        }

        private string GetJson(string _url,string _station)
        {
            string jsonString = "";

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(_url);

            try
            {
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                using (Stream stream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    jsonString = reader.ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                onErrorEvent("GetJson Error - " + ex.Message.ToString(), _station,true);

            }

            return jsonString;
        }

        private void CheckFolder(string _mediaFolder, string _folder)
        {
            string[] dirs = Directory.GetDirectories(_mediaFolder, _folder, SearchOption.TopDirectoryOnly);
            if (dirs.Length == 0)
            {
                Directory.CreateDirectory(_mediaFolder + _folder);
            }

        }

        private void Send(string station, string action, string folder)
        {
            onSendTrigger.Invoke("SHOOT#" + station + "#" + action + "#" + folder);

        }

        private void WriteLog(string str)
        {
            
        }

    }
}
