﻿// Decompiled with JetBrains decompiler
// Type: NFC_WPF_STANDALONE.UDP
// Assembly: NFC_WPF_STANDALONE, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E05B9FBF-9C1C-4435-A8BC-51E6AB750350
// Assembly location: C:\Users\שי\Desktop\פועלים סניפים חכמים\NFC\NFC_MANAGER_WPF_STANDALONE\NFC_WPF_STANDALONE\bin\Debug\NFC_WPF_STANDALONE.exe

using System;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Linq;
using System.Windows.Threading;


namespace ServerApp
{
  internal class UDP
  {

    public delegate void DataRecieved(string data);
    public event DataRecieved onDataRecieved;

    public int ListenPort = 20001;
    private bool done = false;
    private UdpClient listener;
    private IPEndPoint groupEP;
    public string HostName;
    public int transmitPort;
    public int recievePort;
    private Thread thread;

    public int udpSleep;

    Dispatcher dispatcher = Dispatcher.CurrentDispatcher; 

         

    public void StartListening()
    {
      this.listener = new UdpClient(this.ListenPort);
      this.groupEP = new IPEndPoint(IPAddress.Any, this.ListenPort);
      this.thread = new Thread(new ThreadStart(this.WorkThreadFunction));
      this.thread.Start();
    }

    public void StopListening()
    {
      this.listener.Close();
    }

    public void WorkThreadFunction()
    {
      try
      {
        while (!this.done)
        {
         // Console.WriteLine("Waiting for broadcast");
          byte[] bytes = this.listener.Receive(ref this.groupEP);
          string data = Encoding.ASCII.GetString(bytes, 0, bytes.Length);
                    //040094808E

         // Console.WriteLine(data);
          int from;

                    //if (data.Contains("#"))
                    //{
                    //    int n = 1;
                    //    from = data.TakeWhile(c => (n -= (c == '#' ? 1 : 0)) > 0).Count();
                    //    n = 2;
                    //    int to = data.TakeWhile(c => (n -= (c == '#' ? 1 : 0)) > 0).Count();

                    //    dispatcher.Invoke(new Action(() => this.NewData(data.Substring(from + 1, to - from - 1))));

                    //}
                    dispatcher.Invoke(new Action(() => this.NewData(data)));

                }
      }
      catch (Exception ex)
      {
        Console.WriteLine(((object) ex).ToString());
      }
      finally
      {
        this.listener.Close();
        this.listener = new UdpClient(this.ListenPort);
      }
    }

    private void NewData(string d)
    {
       // Console.WriteLine(d);
        if (onDataRecieved != null)
        {
            onDataRecieved(d);
        }
    }
    public void Send(string sDataToSend, string sHostName, int iPortNumber)
    {
      Console.WriteLine("Sending....");
      try
      {
        Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
        socket.EnableBroadcast = true;
        IPAddress address = IPAddress.Parse(sHostName);
        byte[] bytes = Encoding.ASCII.GetBytes(sDataToSend);
        IPEndPoint ipEndPoint = new IPEndPoint(address, iPortNumber);
        socket.SendTo(bytes, (EndPoint) ipEndPoint);
      }
      catch (Exception ex)
      {
      }
    }
  }
}
