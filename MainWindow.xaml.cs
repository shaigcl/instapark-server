﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Reflection;
using System.IO;
using System.IO.Ports;
using System.Windows.Threading;
using System.Threading;
using System.Diagnostics;
using System.Net;
using Newtonsoft.Json;

//TD need to listen to udpSendPort (2002) to get trigger
//RPI need to listen to udpSendPort (2002) to get the trigger
//Server needs to listen to udpListenPort(2001) to get Release msg from TD


//TO DO
//ADD sounds to RPI - button clicks, CountDown, Error, release.
//Check Getting status


namespace ServerApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        private string apiBaseUrl;

        private UDP UdpListener = new UDP();
        private UDP UdpListener2 = new UDP();
        private int udpListenPort = 2001;
        private int udpListenPort2 = 2001;
        private string sendIP = "255.255.255.255";
        private int udpSendPort = 2002; //RPI needs to listen to this port

        private UDP[] RfidListeners = new UDP[6];
        private int[] RfidPorts = new int[6] { 1001, 1002, 1003, 1004, 1005, 1006 };

        private Label[] snapLabels = new Label[6];
        private Label[] boomLabels = new Label[6];
        private string[] stationState = new string[6];
        private TextBox[] RfidText = new TextBox[6];
        private string[] rfidTags = new string[6];

        private Button[] releaseBtns = new Button[6];

        private int[] stationFlg = new int[6] { 0, 0, 0, 0, 0, 0 }; //0 = not in use; 1 = waiting; 2 = in use
        private string[] userFolder = new string[6];

        private ProccessTag[] pTags = new ProccessTag[6];

        System.Windows.Threading.DispatcherTimer CheckQueTimer = new System.Windows.Threading.DispatcherTimer();

        System.Windows.Threading.DispatcherTimer lastStateTimer = new System.Windows.Threading.DispatcherTimer();

        private string mediaFolder;
        
        private List<StationQue> stationQue = new List<StationQue>();

        #region ========================== INITIALIZE ===========================

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
           // exe = Assembly.GetExecutingAssembly().Location;
           // exeDir = System.IO.Path.GetDirectoryName(exe);

            ReadConfig();
            SetLabels();
            SetUDP();
            SetTimers();
            UdpListener.Send("STATUS#", sendIP, udpSendPort);

        }

    

        private void ReadConfig()
        {
            using (StreamReader sr = new StreamReader("config.txt"))
            {
             
                udpListenPort = Convert.ToInt16(ParseConfig(sr.ReadLine()));
                udpListenPort2 = Convert.ToInt16(ParseConfig(sr.ReadLine()));
                udpSendPort = Convert.ToInt16(ParseConfig(sr.ReadLine()));
                for(int i = 0; i< RfidPorts.Length; i++)
                {
                    RfidPorts[i] = Convert.ToInt16(ParseConfig(sr.ReadLine()));
                }
                apiBaseUrl = ParseConfig(sr.ReadLine());
                mediaFolder = ParseConfig(sr.ReadLine());
            }

        }

        private void ReadLastState()
        {
            using (StreamReader sr = new StreamReader("LastState.txt"))
            {
                
               // lastState = sr.ReadLine();
              
            }
        }

        private void SaveLastState()
        {
            using (StreamWriter sw = new StreamWriter("LastState.txt"))
            {

               // sw.WriteLine(lastState);
                
                sw.Flush();
            }
        }

        private string ParseConfig(String str)
        {
            string[] arr = str.Split('='); ;
            return arr[1].Trim();
        }
           
        private void SetTimers()
        {

            CheckQueTimer.Tick += new EventHandler(CheckQueTimer_Tick);
            CheckQueTimer.Interval = new TimeSpan(0, 0, 0,1);

           // lastStateTimer.Tick += new EventHandler(lastStateTimer_Tick);
          //  lastStateTimer.Interval = new TimeSpan(0, 0, lastStateInterval);


        }
        
        private void SetLabels()
        {
            snapLabels[0] = lblSnap1;
            snapLabels[1] = lblSnap2;
            snapLabels[2] = lblSnap3;
            snapLabels[3] = lblSnap4;
            snapLabels[4] = lblSnap5;
            snapLabels[5] = lblSnap6;

            boomLabels[0] = lblBoom1;
            boomLabels[1] = lblBoom2;
            boomLabels[2] = lblBoom3;
            boomLabels[3] = lblBoom4;
            boomLabels[4] = lblBoom5;
            boomLabels[5] = lblBoom6;

            RfidText[0] = txtRfid1;
            RfidText[1] = txtRfid2;
            RfidText[2] = txtRfid3;
            RfidText[3] = txtRfid4;
            RfidText[4] = txtRfid5;
            RfidText[5] = txtRfid6;

            releaseBtns[0] = btnRelease1;
            releaseBtns[1] = btnRelease2;
            releaseBtns[2] = btnRelease3;
            releaseBtns[3] = btnRelease4;
            releaseBtns[4] = btnRelease5;
            releaseBtns[5] = btnRelease6;



            for (int i = 0; i<stationState.Length; i++)
            {
                stationState[i] = "snap";
                releaseBtns[i].Click += ReleaseClicked;
                boomLabels[i].MouseUp += BoomLableClicked;
                snapLabels[i].MouseUp += SnapLableClicked;
                RfidText[i].MouseDoubleClick += DoubleClickText;
               // if (i>0) stationFlg[i] = 2;  ///////////////////////////
            }
        }
       
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            UdpListener.StopListening();
            UdpListener2.StopListening();
            for (int i = 0; i< RfidListeners.Length; i++)
            {
                RfidListeners[i].StopListening();
            }
           
        }

        #endregion

        #region ========================== MANAGER ===========================
        
        private void SetStationState(int stationIndex, string action)
        {
            //Here the server sends RPI + Touch to start count down

            stationState[stationIndex] = action;
            switch (action)
            {
                case "snap":
                    snapLabels[stationIndex].Background = new SolidColorBrush(Colors.LightGreen);
                    boomLabels[stationIndex].Background = new SolidColorBrush(Colors.Transparent);
                    break;
                case "boom":
                    snapLabels[stationIndex].Background = new SolidColorBrush(Colors.Transparent);
                    boomLabels[stationIndex].Background = new SolidColorBrush(Colors.LightGreen);
                    break;
            }

            //if the user chose boom and there are less than 2 booms active or if it chose snap
            bool shootFlg = CheckLimit(stationIndex, rfidTags[stationIndex]);

            if ((shootFlg) || (action == "snap")) 
            {
               // "SHOOT#" + station + "#" + action + "#" + folder
                SendMsg("GetReady#" + (stationIndex+1).ToString() + "#" + action +"#" + pTags[stationIndex].visitorFolder);

            } else if (!shootFlg)//send a Wait msg to station
            {
                SendMsg("Wait#" + stationIndex.ToString());
            }
        }

        private void CreateTrigger(string tag, int station)
        {
            if (stationFlg[station - 1]<2) //checks the station is not busy
            {
                WriteLog("Got tag " + tag, station.ToString());

                RfidText[station-1].Background = new SolidColorBrush(Colors.Red);


               // if (CheckLimit(station, tag))
               // {
                    stationFlg[station - 1] = 2; //in use
                    ProccessTag pt = new ProccessTag();
                    pt.onSendTrigger += SendMsg;
                    pt.onErrorEvent += WriteError;
                    pt.onLogEvent += WriteLog;
                    pt.NewTag(station.ToString(), tag, apiBaseUrl, stationState[station - 1], mediaFolder);
                    pTags[station - 1] = pt;

                   /// ReleaseStation(station - 1, "NULL#", false);
                // }


            } else //if the station is busy - sends an Error msg (and plays an error sound on the RPI)
            {
                SendMsg("ERROR#" + station.ToString());
                
            }
           

        }
        
        private bool CheckLimit(int station, string tag)
        {
            bool flg = true;
           

            if (CountBooms(station) >= 2)    //Needs to delay
            {
                CheckQueTimer.Stop();
                flg = false;
               // SendMsg("WAIT#" + station.ToString());
                StationQue SQ = new StationQue();
                SQ.station = station;
                SQ.tag = tag;
                stationQue.Add(SQ);
                stationFlg[station-1] = 1; //waiting
                CheckQueTimer.Start();
            }

            return flg;
        }

        private int CountBooms(int station)
        {
            int counter = 0;
            for (int i = 0; i < stationState.Length; i++)
            {
                if ((stationState[i] == "boom") && (stationFlg[i] == 2) && ((station - 1) != i))
                {
                    counter++;
                }

            }
            return counter;
        }

        private void CheckQueTimer_Tick(object sender, EventArgs e)
        {
           
           if (stationQue.Count > 0)
            {
                if (CountBooms(stationQue[0].station) < 2)
                {

                    CreateTrigger(stationQue[0].tag, stationQue[0].station);
                    stationQue.RemoveAt(0);
                }
                    
               
            }

        }

        private void ReleaseClicked(object sender, RoutedEventArgs e)
        {
            Button btn = (Button)sender;
            int index = Convert.ToInt16(btn.Name.Substring(btn.Name.Length - 1, 1));
            ReleaseStation(index-1, "RELEASE#" + index.ToString(),true);
        }

        private void WriteError(string msg,string station,bool releaseFlg)
        {
            listLog.Items.Add(DateTime.Now + ": " + "station: " + station + " - " + msg);

            SendMsg("ERROR#" + station);

            if (releaseFlg)
            {
                ReleaseStation(Convert.ToInt16(station) - 1, "NULL#", false);
            }
           
        }
        private void WriteLog(string msg, string station)
        {
            listLog.Items.Add(DateTime.Now + ": " + "station: " + station + " - " + msg);
            
        }

        #endregion

        #region ========================== UDP ===========================

        private void SetUDP()
        {
            UdpListener.onDataRecieved += UdpDataRecieved;
            UdpListener.HostName = "255.255.255.255";
            UdpListener.ListenPort = udpListenPort;
            UdpListener.StartListening();

            UdpListener2.onDataRecieved += UdpDataRecieved;
            UdpListener2.HostName = "255.255.255.255";
            UdpListener2.ListenPort = udpListenPort2;
            UdpListener2.StartListening();

            int i;
            for (i = 0; i < RfidListeners.Length; i++)
            {
                RfidListeners[i] = new UDP();
            }
                
            RfidListeners[0].onDataRecieved += RfidDataRecieved1;
            RfidListeners[1].onDataRecieved += RfidDataRecieved2;
            RfidListeners[2].onDataRecieved += RfidDataRecieved3;
            RfidListeners[3].onDataRecieved += RfidDataRecieved4;
            RfidListeners[4].onDataRecieved += RfidDataRecieved5;
            RfidListeners[5].onDataRecieved += RfidDataRecieved6;

            for (i = 0; i< RfidListeners.Length; i++)
            {
                RfidListeners[i].HostName = "255.255.255.255";
                RfidListeners[i].ListenPort = RfidPorts[i];
                RfidListeners[i].StartListening();
                rfidTags[i] = "";
                RfidText[i].Text = "";
            }
           
           
        }

        private void RfidDataRecieved1(string tag)
        {
            rfidTags[0] += tag;
            GotTag(0);
                      
        }
        private void RfidDataRecieved2(string tag)
        {
            rfidTags[1] += tag;
            GotTag(1);
        }
        private void RfidDataRecieved3(string tag)
        {
            rfidTags[2] += tag;
            GotTag(2);
            
        }
        private void RfidDataRecieved4(string tag)
        {
            rfidTags[3] += tag;
            GotTag(3);
        }
        private void RfidDataRecieved5(string tag)
        {
            rfidTags[4] += tag;
            GotTag(4);
        }
        private void RfidDataRecieved6(string tag)
        {
            rfidTags[5] += tag;
            GotTag(5);
        }

        private void GotTag(int index)
        {
           // if (rfidTags[index].Contains("\u0003"))
           // {
                RfidText[index].Text = rfidTags[index];
                rfidTags[index] = rfidTags[index].Replace("\r\n", "");
                rfidTags[index] = rfidTags[index].Replace("\u0002", "");
                rfidTags[index] = rfidTags[index].Replace("\u0003", "");
                CreateTrigger(rfidTags[index], index + 1);
                //rfidTags[index] = "";
          //  }
        }

        private void UdpDataRecieved(string data)
        {
            listLog.Items.Add("UDP Recieved: " + data);

            string[] arr = data.Split('#');
            try
            {
                switch (arr[0])
                {
                    case "STAT":
                        
                        SetStationState(Convert.ToInt16(arr[1]) - 1, arr[2]);
                        break;
                    case "BTN":
                        SetStationState(Convert.ToInt16(arr[1])-1, arr[2]);
                        break;
                    case "RELEASE": //Got Release from TD - send to the relevant RPI
                        int index = Convert.ToInt16(arr[1]) - 1;
                        ReleaseStation(index,data,false);
                        break;
                }
            }
            catch (Exception)
            {

                throw;
            }
            
           


        }

        private void ReleaseStation(int index,string data,bool clickedFlg)
        {
            stationFlg[index] = 0;
            RfidText[index].Background = new SolidColorBrush(Colors.White);
            if (clickedFlg)
            {
               SendMsg(data);
            }
            
        }

        private void lastStateTimer_Tick(object sender, EventArgs e)
        {
            lastStateTimer.Stop();
           


        }

        private void SendMsg(string msg)
        {
            WriteLog("Sent UDP Msg :" + msg, "");
            UdpListener.Send(msg, sendIP, udpSendPort);
        }


        #endregion

        #region ========================== DEBUG ===========================

        //Debug a RFID read:
        private void DoubleClickText(object sender, RoutedEventArgs e)
        {
            TextBox txt = (TextBox)sender;
            int station = Convert.ToInt16(txt.Name.Substring(txt.Name.Length - 1, 1));
            CreateTrigger("567df26hba", station);
        }

        private void BoomLableClicked(object sender, RoutedEventArgs e)
        {
            SetManualStatus(sender, "boom");
        }

        private void SnapLableClicked(object sender, RoutedEventArgs e)
        {
            SetManualStatus(sender, "snap");
        }

        private void SetManualStatus(object obj, string type)
        {
            Label lbl = (Label)obj;
            int station = Convert.ToInt16(lbl.Name.Substring(lbl.Name.Length - 1, 1));
            SendMsg("SetStatus#" + station.ToString() + "#" + type);
            SetStationState(station - 1, type);

        }
        private void BtnListClear_Click(object sender, RoutedEventArgs e)
        {
            listLog.Items.Clear();

        }

        #endregion


    }

    public class StationQue
    {
        public int station;
        public string tag;
    }

    public static class SendKeys
    {
        /// <summary>
        ///   Sends the specified key.
        /// </summary>
        /// <param name="key">The key.</param>
        public static void Send(Key key)
        {
            if (Keyboard.PrimaryDevice != null)
            {
                if (Keyboard.PrimaryDevice.ActiveSource != null)
                {
                    var e1 = new KeyEventArgs(Keyboard.PrimaryDevice, Keyboard.PrimaryDevice.ActiveSource, 0,key) { RoutedEvent = Keyboard.KeyDownEvent };
                    InputManager.Current.ProcessInput(e1);
                }
            }
        }
    }
}


//private void SendSMS()
//{
//    string stringResponse = "";
//    string msg1 = "תודה על ביקורך בפארק, לחץ על הלינק הבא כדי לצפות בתמונות: ";
//    string msg2 = "http://apps.creativelabs.co.il/instapark/?page=park&q=";
//    string msg3 = "0558859545567ghq8";
//    string phone = "0505673203";
//    string from = "Instapark";

//    string address = string.Format("http://www.micropay.co.il/ExtApi/ScheduleSms.php?get=1&charset=iso-8859-8&uid=3709&un=inhouse&msg={0}&from={1}&list={2}",
//    Uri.EscapeDataString(msg1 + msg2 + msg3),
//    Uri.EscapeDataString(from),
//    Uri.EscapeDataString(phone));


//    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(address);

//    try
//    {
//        HttpWebResponse response = (HttpWebResponse)request.GetResponse();
//        using (Stream stream = response.GetResponseStream())
//        using (StreamReader reader = new StreamReader(stream))
//        {
//            stringResponse = reader.ReadToEnd();
//        }
//    }
//    catch (Exception ex)
//    {


//    }


//}
